require 'rspec'
require_relative 'crawler'

describe 'Crawler' do
  crawler = Crawler.new()
  it 'should return an array' do
    expect(crawler.find_files_by_filename("test")).to be_an_instance_of(Array)
  end

  it 'should find some files' do
    expect(crawler.find_files_by_filename('test')).to all( be_a(String).and include('test'))
  end

  it 'should find all exe files' do
    expect(crawler.find_files_by_filename('^(.*)\.exe$')).to all( be_a(String).and include('.exe'))
  end
end