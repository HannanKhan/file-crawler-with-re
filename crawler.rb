class Crawler

  def find_files(path = "C:\\Users\\hannan\\Desktop\\6th Sem", depth= 3)

    if depth == 0
      return []
    end

    directories = Dir.entries(path).select do |entry|
      File.directory? File.join(path, entry) and !(entry =='.' || entry == '..')
    end

    files = Dir.entries(path).select do |entry|
      !(File.directory? File.join(path, entry)) and !(entry =='.' || entry == '..')
    end

    files.map! { |file| File.join(path, file)}

    directories.map {|dir| File.join(path, dir)}.each do |dir|
      find_files(dir, depth - 1).each { |file| files.push(file)}
    end

    files

  end

  def find_string_in_files(path = "C:\\Users\\hannan\\Desktop\\6th Sem", depth= 3, to_find)

    self.find_files(path, depth).select do |entry|
      if File.extname(entry) == '.txt'
        lines = []
        File.foreach(entry) {|line| lines.push line }
        !(lines.join(' ').match(to_find)).nil?
      else
        false
      end
    end

  end

  def find_files_on_last_modified_date(path = "C:\\Users\\hannan\\Desktop\\6th Sem", depth= 3, to_find)

    self.find_files(path, depth).select do |entry|
      File.mtime(entry).to_s().split(' ')[0] == to_find
    end
  end

  def find_files_on_creation_date(path = "C:\\Users\\hannan\\Desktop\\6th Sem", depth= 3, to_find)

    self.find_files(path, depth).select do |entry|
      File.ctime(entry).to_s().split(' ')[0] == to_find
    end
  end

  def find_files_by_size(path = "C:\\Users\\hannan\\Desktop\\6th Sem", depth= 3, min_size = 0, max_size)

    self.find_files(path, depth).select do |entry|
      File.stat(entry).size >= min_size and File.stat(entry).size <= max_size
    end
  end

  def find_files_by_filename(path = "C:\\Users\\hannan\\Desktop\\6th Sem", depth= 3, to_find)

    self.find_files(path, depth).select do |entry|
      File.basename(entry).match(to_find)
    end
  end
end